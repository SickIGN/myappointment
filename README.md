# MyAppointment

Healthcare Appointment Booking System

## Specifications

Python 3.8.0

Django 3.2.3

## Pre-requisites

### 1. Python

Please install python from the following [link](https://www.python.org/ftp/python/3.8.0/python-3.8.0-amd64.exe)

### 2. Python pip

Pip should come packaged together with python installation.

To check if pip is installed, run "pip --version".

If in case of missing pip package:

1. Download from [link](https://bootstrap.pypa.io/get-pip.py).
2. Start command prompt or bash and "cd" to get-pip.py location.
3. Run python get-pip.py

### 3. Requirements

Please run following command to download required packages:

1. pip install -r requirements.txt

## Starting server

1. Open "cmd" or "bash"
2. "cd" to project root directory -> MyAppointment
3. Run "py manage.py runserver"
4. Open any web browser and enter "http://127.0.0.1:8000/" to view page

or

### run project in virtual environment

1. Open "cmd" or "bash"
2. "cd" to project root directory -> myappointment
3. "cd" to /venv/Script/activate
4. "cd" to "MyAppointment"
5. Run "py manage.py runserver"
6. Open any web browser and enter "http://127.0.0.1:8000/" to view page
