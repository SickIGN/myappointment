from django.urls import path
from . import views

app_name = 'MyAppointmentBookingSystem'
urlpatterns = [
    path('', views.index, name='index'),
]
