from typing import ContextManager
from django.shortcuts import render
from .models import *

# Create your views here.
def index(request):
    page_title = "MyAppointment - Home"
    context = {
        "page_title": page_title,
    }
    return render(request, 'MyAppointmentBookingSystem/index.html', context)