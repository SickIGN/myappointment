from django.shortcuts import render, redirect
from .forms import NewUserForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm

# Create your views here.
def register_request(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration successful.")
            return redirect("MyAppointmentBookingSystem:index")
        messages.error(request, "Unsuccessful registration. Invalid information.")
    form = NewUserForm
    page_title = "MyAppointment - Register"
    context={
        "register_form":form,
        "page_title":page_title
        }
    return render(request, "account/register.html", context)

def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None: #after verified correct user, login user
                login(request,user)
                messages.info(request, f"You are now logged in as {username}.")
                return redirect("MyAppointmentBookingSystem:index")
            else:
                messages.error(request, "Invalid username or password.")
        else: # form is not valid
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    page_title = "MyAppointment - Login"
    context = {
        "login_form":form,
        "page_title":page_title
        }
    return render(request, "account/login.html", context)

def logout_request(request):
    logout(request)
    messages.info(request, "You have successfully logged out.")
    return redirect("MyAppointmentBookingSystem:index")